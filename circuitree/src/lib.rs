#![cfg_attr(feature = "metrics_bench", feature(get_mut_unchecked))]
use ark_ff::fields::PrimeField;
use ark_r1cs_std::prelude::*;
use ark_relations::r1cs::{ConstraintSynthesizer, ConstraintSystemRef, SynthesisError};
use datalog::parsing::{Argument, Atom, ComparisonOperator, Data, Question, Variable};
use datalog::reasoning::{
    Assertion, DataType, History, Iteration, ReasonerRule, Ruleset, UnificationLocation,
};
use itertools::Itertools;
use std::iter;

#[cfg(feature = "metrics_bench")]
use {crate::utils::metrics::Metrics, std::rc::Rc};

pub mod utils;

pub type DATA = u32;
#[allow(non_camel_case_types)]
pub type ZK_DATA<F> = UInt32<F>;
pub const DATA_LENGTH: usize = DATA::BITS as usize;
const DUMMY_ARG: DATA = 1;
const DATATYPE_ATOM: DATA = 8;
const DATATYPE_NUMBER: DATA = 9;

fn zk_dummy<F: PrimeField>() -> ZK_DATA<F> {
    ZK_DATA::<F>::constant(DUMMY_ARG)
}

/// Extract DATA representation from DataType or its items
trait Datafiable {
    fn datafy(&self) -> (DATA, DATA);
}

impl Datafiable for Atom<DATA> {
    fn datafy(&self) -> (DATA, DATA) {
        (DATATYPE_ATOM, self.name)
    }
}

impl Datafiable for DATA {
    fn datafy(&self) -> (DATA, DATA) {
        (DATATYPE_NUMBER, *self)
    }
}

impl Datafiable for DataType<DATA> {
    fn datafy(&self) -> (DATA, DATA) {
        match self {
            DataType::Atom(atm) => atm.datafy(),
            DataType::Number(n) => (*n as DATA).datafy(),
        }
    }
}

pub fn dummy_assertion() -> Assertion<DATA> {
    Assertion {
        parents: None,
        predicate: DUMMY_ARG,
        arguments: vec![],
    }
}

pub trait ContainsAssertions {
    fn max_arguments(&self) -> Option<usize>;
}
impl<Var: Data, T: Data> ContainsAssertions for Ruleset<Var, T> {
    fn max_arguments(&self) -> Option<usize> {
        self.iter()
            .flat_map(|r| {
                r.logic_subgoals
                    .iter()
                    .map(|sg| sg.arguments.len())
                    .chain(iter::once(r.head.arguments.len()))
            })
            .max()
    }
}

impl<T: Data> ContainsAssertions for Iteration<T> {
    fn max_arguments(&self) -> Option<usize> {
        self.assertions.iter().map(|a| a.arguments.len()).max()
    }
}

impl<T: Data> ContainsAssertions for History<T> {
    fn max_arguments(&self) -> Option<usize> {
        self.iterations
            .iter()
            .filter_map(Iteration::max_arguments)
            .max()
    }
}

pub fn get_max_arguments<Var: Data, T: Data>(
    history: &History<T>,
    ruleset: &Ruleset<Var, T>,
) -> usize {
    let rules_args = ruleset.max_arguments().unwrap_or(0);
    let history_args = history.max_arguments().unwrap_or(0);
    usize::max(rules_args, history_args)
}

/// Warning: These comparisons are not yet safe to use!
/// Crucially: our numbers are not allowed to use the MSB
fn greater_than<F: PrimeField>(
    lhs: ZK_DATA<F>,
    rhs: ZK_DATA<F>,
) -> Result<Boolean<F>, SynthesisError> {
    // We do rhs - lhs, then return the overflow bit
    // Negate lhs
    let lhs_inv = lhs ^ &ZK_DATA::constant(DATA::MAX);
    // rhs - (lhs_inv + 1)
    let res = ZK_DATA::wrapping_add_many(&vec![rhs, lhs_inv, ZK_DATA::constant(1)])?;
    let bits = res.to_bits_le()?;
    let underflow = bits.last().unwrap();
    Ok(underflow.clone())
}

fn greater_than_or_equal<F: PrimeField>(
    lhs: ZK_DATA<F>,
    rhs: ZK_DATA<F>,
) -> Result<Boolean<F>, SynthesisError> {
    // Check Equality
    let eq = lhs.is_eq(&rhs)?;
    // We do rhs - lhs, then return the overflow bit
    // Negate lhs
    let lhs_inv = lhs ^ &ZK_DATA::constant(DATA::MAX);
    // rhs - (lhs_inv + 1)
    let res = ZK_DATA::wrapping_add_many(&vec![rhs, lhs_inv, ZK_DATA::constant(1)])?;
    let bits = res.to_bits_le()?;
    let underflow = bits.last().unwrap();
    // Or equality
    let gte = eq | underflow;
    Ok(gte)
}

#[derive(Clone, Debug)]
pub struct ZKFact<F: PrimeField> {
    pub valid_witness: Option<bool>,
    pub valid: Boolean<F>,
    pub predicate_witness: Option<DATA>,
    pub predicate: ZK_DATA<F>,
    pub arguments_witness: Option<Vec<(DATA, DATA)>>,
    pub arguments: Vec<(ZK_DATA<F>, ZK_DATA<F>)>,
}

impl<F: PrimeField> ZKFact<F> {
    pub fn dummy(arg: usize) -> Self {
        Self {
            valid_witness: Some(true),
            valid: Boolean::constant(true),
            predicate_witness: Some(DUMMY_ARG),
            predicate: ZK_DATA::constant(DUMMY_ARG),
            arguments_witness: Some((0..arg).map(|_| (DUMMY_ARG, DUMMY_ARG)).collect_vec()),
            arguments: (0..arg).map(|_| (zk_dummy(), zk_dummy())).collect_vec(),
        }
    }
    pub fn new_without_witness(
        valid: Boolean<F>,
        predicate: ZK_DATA<F>,
        arguments: Vec<(ZK_DATA<F>, ZK_DATA<F>)>,
    ) -> Self {
        Self {
            valid_witness: None,
            valid,
            predicate_witness: None,
            predicate,
            arguments_witness: None,
            arguments,
        }
    }

    /// Allocated as: [valid, predicate, {argument_type, argument_data}*]
    /// Valid does technically not need to be allocated, since it is always true.
    pub fn allocate_from_reasoner_assertion(
        cs: &ConstraintSystemRef<F>,
        fact: &Assertion<DATA>,
        most_arguments: usize,
    ) -> Result<Self, SynthesisError> {
        let valid_witness = true;
        let valid = Boolean::constant(true);
        let predicate =
            ZK_DATA::new_witness(ark_relations::ns!(cs, "p_pred"), || Ok(fact.predicate))?;
        let arguments_witness = fact
            .arguments
            .iter()
            .map(DataType::datafy)
            .chain(iter::repeat((DUMMY_ARG, DUMMY_ARG)))
            .take(most_arguments)
            .collect_vec();
        let arguments = arguments_witness
            .iter()
            .map(|(t, d)| {
                let zkt = ZK_DATA::new_witness(ark_relations::ns!(cs, "p_arg_t"), || Ok(t))?;
                let zkd = ZK_DATA::new_witness(ark_relations::ns!(cs, "p_arg_d"), || Ok(d))?;
                Ok((zkt, zkd))
            })
            .collect::<Result<Vec<(ZK_DATA<F>, ZK_DATA<F>)>, SynthesisError>>()?;
        Ok(Self {
            valid_witness: Some(valid_witness),
            valid,
            predicate_witness: Some(fact.predicate),
            predicate,
            arguments_witness: Some(arguments_witness),
            arguments,
        })
    }

    pub fn allocate_without_witness(
        cs: &ConstraintSystemRef<F>,
        most_arguments: usize,
    ) -> Result<Self, SynthesisError> {
        let valid = Boolean::constant(true);
        let predicate =
            ZK_DATA::new_witness(ark_relations::ns!(cs, "v_pred"), || -> Result<DATA, _> {
                Err(SynthesisError::AssignmentMissing)
            })?;
        let arguments = (0..(Self::size_in_multipliers(most_arguments) - 1))
            .map(|_| {
                let zkt = ZK_DATA::new_witness(
                    ark_relations::ns!(cs, "v_arg_t"),
                    || -> Result<DATA, _> { Err(SynthesisError::AssignmentMissing) },
                )?;
                let zkd = ZK_DATA::new_witness(
                    ark_relations::ns!(cs, "v_arg_d"),
                    || -> Result<DATA, _> { Err(SynthesisError::AssignmentMissing) },
                )?;
                Ok((zkt, zkd))
            })
            .collect::<Result<Vec<(ZK_DATA<F>, ZK_DATA<F>)>, SynthesisError>>()?;
        Ok(Self {
            valid_witness: None,
            valid,
            predicate_witness: None,
            predicate,
            arguments_witness: None,
            arguments,
        })
    }

    // TODO: Can constants be used here?
    pub fn from_reasoner_assertion_with_dummies(fact: &Assertion<DATA>, args: usize) -> Self {
        let arguments = fact
            .arguments
            .iter()
            .map(DataType::datafy)
            .chain(iter::repeat((DUMMY_ARG, DUMMY_ARG)))
            .map(|(t, d)| {
                let zkt = ZK_DATA::constant(t);
                let zkd = ZK_DATA::constant(d);
                (zkt, zkd)
            })
            .take(args)
            .collect_vec();
        let arguments_witness = fact
            .arguments
            .iter()
            .map(DataType::datafy)
            .chain(iter::repeat((DUMMY_ARG, DUMMY_ARG)))
            .take(args)
            .collect_vec();
        let valid_witness = true;
        let valid = Boolean::constant(true);
        let predicate = ZK_DATA::constant(fact.predicate);
        Self {
            valid_witness: Some(valid_witness),
            valid,
            predicate_witness: Some(fact.predicate),
            predicate,
            arguments_witness: Some(arguments_witness),
            arguments,
        }
    }

    pub fn from_reasoner_question_with_dummies(
        question: &Question<String, DATA>,
        args: usize,
    ) -> Self {
        let arguments_witness = question
            .arguments
            .iter()
            .map(|f| match f {
                Argument::Atom(atm) => atm.datafy(),
                Argument::Variable(_) => unimplemented!("Variable in question not implemented"),
                Argument::Number(_) => unimplemented!("Arithmetic in question not unimplemented"),
            })
            .chain(iter::repeat((DUMMY_ARG, DUMMY_ARG)))
            .take(args)
            .collect_vec();
        let arguments = arguments_witness
            .iter()
            .map(|(t, d)| {
                let zkt = ZK_DATA::constant(*t);
                let zkd = ZK_DATA::constant(*d);
                (zkt, zkd)
            })
            .collect_vec();
        let valid_witness = true;
        let valid = Boolean::constant(true);
        Self {
            valid_witness: Some(valid_witness),
            valid,
            predicate_witness: Some(question.predicate),
            predicate: ZK_DATA::constant(question.predicate),
            arguments_witness: Some(arguments_witness),
            arguments,
        }
    }

    pub fn size_in_multipliers(arguments: usize) -> usize {
        //(Valid x Predicate) + (Argument Type x Argument Value)*
        1 + arguments
    }
}

#[derive(Copy, Clone)]
pub struct IterationMetadata {
    iterations: usize,
    largest_assertion_set_size: usize,
    pub most_arguments: usize,
}

impl IterationMetadata {
    /// Necessary because of trusted setup
    pub fn from_history(history: &History<DATA>, rules: &Ruleset<String, DATA>) -> Self {
        let iterations = history.iterations.len();
        let largest_assertion_set_size = history.largest_assertion_set_size;
        let most_arguments = get_max_arguments(history, rules);
        Self {
            iterations,
            largest_assertion_set_size,
            most_arguments,
        }
    }
}
pub struct IterationData<F: PrimeField> {
    pub meta: IterationMetadata,
    pub assertions: Vec<Vec<ZKFact<F>>>,
}

impl<F: PrimeField> IterationData<F> {
    pub fn allocate_from_history(
        prover: &ConstraintSystemRef<F>,
        history: &History<DATA>,
        rules: &Ruleset<String, DATA>,
    ) -> Result<Self, SynthesisError> {
        let iterations = history.iterations.len();
        let largest_assertion_set_size = history.largest_assertion_set_size;
        let most_arguments = get_max_arguments(history, rules);
        let assertions = history
            .iterations
            .iter()
            .map(|i| {
                i.assertions
                    .iter()
                    .chain(iter::repeat(&dummy_assertion()))
                    .take(largest_assertion_set_size)
                    .map(|a| ZKFact::allocate_from_reasoner_assertion(prover, a, most_arguments))
                    .collect()
            })
            .collect::<Result<Vec<Vec<ZKFact<F>>>, SynthesisError>>()?;
        let meta = IterationMetadata {
            iterations,
            largest_assertion_set_size,
            most_arguments,
        };
        Ok(Self { meta, assertions })
    }

    pub fn allocate_from_metadata(
        verifier: &ConstraintSystemRef<F>,
        meta: &IterationMetadata,
    ) -> Result<Self, SynthesisError> {
        let assertions = (0..meta.iterations)
            .map(|_| {
                (0..meta.largest_assertion_set_size)
                    .map(|_| ZKFact::allocate_without_witness(verifier, meta.most_arguments))
                    .collect()
            })
            .collect::<Result<Vec<Vec<ZKFact<F>>>, SynthesisError>>()?;
        Ok(Self {
            meta: *meta,
            assertions,
        })
    }
}

pub struct InputData<F: PrimeField> {
    pub arguments: usize,
    pub assertions: Vec<ZKFact<F>>,
}

impl<F: PrimeField> InputData<F> {
    pub fn from_history(history: &History<DATA>, max_args: usize) -> Self {
        let facts = history
            .leafs
            .iter()
            .map(|a| ZKFact::from_reasoner_assertion_with_dummies(a, max_args))
            .collect_vec();
        Self {
            arguments: facts.len(),
            assertions: facts,
        }
    }
}

pub enum AllocationData {
    History(History<DATA>),
    Metadata(IterationMetadata),
}

pub struct AllocationGadget {}

impl AllocationGadget {
    pub fn allocate<F: PrimeField>(
        self,
        cs: &ConstraintSystemRef<F>,
        allocation_data: &AllocationData,
        rules: Ruleset<String, DATA>,
    ) -> Result<FirstIterationInInputGadget<F>, SynthesisError> {
        //Allocate Variables
        let iteration_data = match &allocation_data {
            AllocationData::History(history) => {
                IterationData::allocate_from_history(cs, history, &rules)
            }
            AllocationData::Metadata(meta) => IterationData::allocate_from_metadata(cs, meta),
        }?;
        Ok(FirstIterationInInputGadget {
            iteration_data,
            _rules: rules,
        })
    }
}

pub struct FirstIterationInInputGadget<F: PrimeField> {
    iteration_data: IterationData<F>,
    _rules: Ruleset<String, DATA>,
}

impl<F: PrimeField> FirstIterationInInputGadget<F> {
    /// Check whether all elements from the first iteration appear in the initial assertion set
    pub fn constrain_first_iteration_in_input(
        self,
        input: InputData<F>,
    ) -> Result<IterationsGadget<F>, SynthesisError> {
        for fact0 in self.iteration_data.assertions.first().unwrap() {
            let p = input
                .assertions
                .iter()
                //At least one has pair has to match -> or
                .fold(Ok(Boolean::constant(false)), |acc, fact1| {
                    let is_equal = ReasonerProof::compare_facts(fact0, fact1)?;
                    let is_dummy = ReasonerProof::is_dummy_fact(fact0)?;
                    let equal_or_dummy = is_equal | &is_dummy;
                    acc.map(|x| x | &equal_or_dummy)
                })?;
            p.enforce_equal(&Boolean::constant(true))?;
        }
        Ok(IterationsGadget {
            input,
            iteration_data: self.iteration_data,
            rules: self._rules,
        })
    }
}

pub struct IterationsGadget<F: PrimeField> {
    input: InputData<F>,
    iteration_data: IterationData<F>,
    rules: Ruleset<String, DATA>,
}

impl<F: PrimeField> IterationsGadget<F> {
    /// Check subsequent iterations
    pub fn constrain_iteration_entailment(self) -> Result<QueryPresentGadget<F>, SynthesisError> {
        for (assertion_set, result_assertions) in
            self.iteration_data.assertions.iter().tuple_windows()
        {
            self.reasoner_iteration(assertion_set, result_assertions)?
        }
        Ok(QueryPresentGadget {
            iteration_data: self.iteration_data,
        })
    }

    fn reasoner_iteration(
        &self,
        assertion_set: &[ZKFact<F>],
        result_assertion_set: &[ZKFact<F>],
    ) -> Result<(), SynthesisError> {
        //Calculate all new assertions
        let mut new_assertions = Vec::new();
        for (nr_sub, grouped_rules) in self
            .rules
            .iter()
            .map(|r| (r.logic_subgoals.len(), r))
            .into_group_map()
            .iter()
            // into_group_map does not give us a fixed ordering by key
            .sorted_by_key(|(n, _)| **n)
        {
            let assertion_groups = assertion_set.windows(*nr_sub);

            // into_group_map luckily retains the ordering of the original iterator for the value.
            // grouped_rules.sort_by_key(|rule| rule.);

            for subgoals in assertion_groups {
                let mut apply_res = grouped_rules
                    .iter()
                    .map(|r| self.apply_rule(r, subgoals))
                    .collect::<Result<Vec<ZKFact<F>>, _>>()?;
                new_assertions.append(&mut apply_res);
            }
        }
        //Perform the multiplications
        for fact1 in result_assertion_set {
            //For each given assertion
            let is_dummy = ReasonerProof::is_dummy_fact(fact1)?;
            let constraint = new_assertions
                .iter()
                .chain(self.input.assertions.iter())
                .fold(Ok(is_dummy), |acc, fact0| {
                    let diff = ReasonerProof::compare_facts(fact0, fact1)?;
                    acc.map(|x| x | &diff)
                })?;
            constraint.enforce_equal(&Boolean::constant(true))?;
        }
        Ok(())
    }

    fn look_up_variable_value(
        var: &Variable<String>,
        rule: &ReasonerRule<String, DATA>,
    ) -> (usize, usize) {
        *rule
            .indices
            .locations
            .iter()
            .find_map(|(v, x)| {
                if v == var {
                    Some(
                        x.iter()
                            .find_map(|x| match x {
                                UnificationLocation::Head(_) => None,
                                UnificationLocation::LogicSubgoal(idxs)
                                | UnificationLocation::ArithmeticSubgoal(idxs) => Some(idxs),
                            })
                            .unwrap(),
                    )
                } else {
                    None
                }
            })
            .unwrap()
    }

    fn apply_rule(
        &self,
        rule: &ReasonerRule<String, DATA>,
        logic_subgoals: &[ZKFact<F>],
    ) -> Result<ZKFact<F>, SynthesisError> {
        // Check whether or not the logic subgoals are correct
        let tail_values = rule.indices.locations.iter().map(|(_, locations)| {
            locations
                .iter() //Get all tail LCs
                .filter_map(|l| {
                    if let UnificationLocation::LogicSubgoal((i, j)) = l {
                        Some(logic_subgoals[*i].arguments[*j].clone())
                    } else {
                        None
                    }
                })
        });
        let matches_rule = tail_values
            .flat_map(
                |elmts| {
                    let mut elmts = elmts.peekable();
                    let first = elmts.peek().unwrap().clone();
                    elmts.flat_map(move |elmt| vec![elmt.0.is_eq(&first.0), elmt.1.is_eq(&first.1)])
                }, //Check argument positions
            )
            .chain(
                rule.logic_subgoals
                    .iter()
                    // XXX check lengths?
                    .zip(logic_subgoals.iter())
                    .map(|(t, a)| ZK_DATA::constant(t.predicate).is_eq(&a.predicate)),
            );
        // Arithmetic subgoals
        // TODO: code duplication
        let arith_rules_correct =
            rule.arithmetic_subgoals
                .iter()
                .fold(Ok(Boolean::constant(true)), |acc, sg| {
                    let (lhs_type, lhs) = match &sg.lhs {
                        Argument::<String, DATA>::Atom(_) => panic!("Atom in arithmetic subgoal"),
                        Argument::<String, DATA>::Number(n) => (
                            ZK_DATA::constant(DATATYPE_NUMBER),
                            ZK_DATA::constant(*n as DATA),
                        ),
                        Argument::<String, DATA>::Variable(var) => {
                            let (x, y) = Self::look_up_variable_value(var, rule);
                            logic_subgoals[x].arguments[y].clone()
                        }
                    };
                    let (rhs_type, rhs) = match &sg.rhs {
                        Argument::<String, DATA>::Atom(_) => panic!("Atom in arithmetic subgoal"),
                        Argument::<String, DATA>::Number(n) => (
                            ZK_DATA::constant(DATATYPE_NUMBER),
                            ZK_DATA::constant(*n as DATA),
                        ),
                        Argument::<String, DATA>::Variable(var) => {
                            let (x, y) = Self::look_up_variable_value(var, rule);
                            logic_subgoals[x].arguments[y].clone()
                        }
                    };
                    let comp = match sg.operator {
                        ComparisonOperator::Lt => greater_than(rhs, lhs),
                        ComparisonOperator::Gt => greater_than(lhs, rhs),
                        ComparisonOperator::Lte => greater_than_or_equal(rhs, lhs),
                        ComparisonOperator::Gte => greater_than_or_equal(lhs, rhs),
                    };
                    let lhs_nrs = lhs_type.is_eq(&ZK_DATA::constant(DATATYPE_NUMBER))?;
                    let rhs_nrs = rhs_type.is_eq(&ZK_DATA::constant(DATATYPE_NUMBER))?;
                    Ok(vec![acc?, comp?, lhs_nrs, rhs_nrs]
                        .iter()
                        .fold(Boolean::constant(true), |acc, f| acc & f))
                })?;
        // Add arithmetic rules to validity flag
        let matches_rule = matches_rule
            .chain(iter::once(Ok(arith_rules_correct)))
            .fold(Ok(Boolean::constant(true)), |acc, f| {
                acc.and_then(|x| f.map(|y| x & &y))
            })?;
        // Get arguments values
        let new_fact_arguments = rule
            .indices
            .locations
            .iter()
            .filter_map(|(_, locations)| {
                if let Some(UnificationLocation::Head(h)) = locations.iter().find(|e| match e {
                    UnificationLocation::Head(_) => true,
                    UnificationLocation::LogicSubgoal(_)
                    | UnificationLocation::ArithmeticSubgoal(_) => false,
                }) {
                    let loc = locations
                        .iter()
                        .find_map(|e| match e {
                            UnificationLocation::Head(_) => None,
                            UnificationLocation::LogicSubgoal(x)
                            | UnificationLocation::ArithmeticSubgoal(x) => Some(x),
                        })
                        .unwrap();
                    Some((h, loc))
                } else {
                    None
                }
            })
            .sorted_by_key(|(h, _)| **h) //TODO: check that all arguments are there
            .map(|(_, (i, j))| logic_subgoals[*i].arguments[*j].clone())
            .collect_vec();
        // Don't need to check argument validity, since they come from a fresh set
        Ok(ZKFact::new_without_witness(
            matches_rule,
            ZK_DATA::constant(rule.head.predicate),
            new_fact_arguments,
        ))
    }
}

pub struct QueryPresentGadget<F: PrimeField> {
    iteration_data: IterationData<F>,
}

impl<F: PrimeField> QueryPresentGadget<F> {
    fn constrain_query_present(
        self,
        ptquery: Question<String, DATA>,
    ) -> Result<IterationMetadata, SynthesisError> {
        //Make ZKFact from Query
        let query = &ZKFact::from_reasoner_question_with_dummies(
            &ptquery,
            self.iteration_data.meta.most_arguments,
        );
        //Check if query is present in last iteration
        let query_comps = self
            .iteration_data
            .assertions
            .iter()
            .last()
            .unwrap()
            .iter()
            .map(|f| ReasonerProof::compare_facts(f, query))
            .collect::<Result<Vec<Boolean<F>>, _>>()?;
        let query_present = query_comps
            .into_iter()
            .fold(Boolean::constant(false), |acc, f| acc | &f);
        query_present.enforce_equal(&Boolean::constant(true))?;
        Ok(self.iteration_data.meta)
    }
}

pub struct ReasonerProof<F: PrimeField> {
    pub input: InputData<F>,
    pub ptquery: Question<String, DATA>,
    pub rules: Ruleset<String, DATA>,
    pub allocation_data: AllocationData,
    #[cfg(feature = "metrics_bench")]
    pub metrics: Rc<Metrics>,
}

#[cfg(not(feature = "metrics_bench"))]
impl<F: PrimeField> ConstraintSynthesizer<F> for ReasonerProof<F> {
    fn generate_constraints(self, cs: ConstraintSystemRef<F>) -> ark_relations::r1cs::Result<()> {
        AllocationGadget {}
            .allocate(&cs, &self.allocation_data, self.rules)?
            .constrain_first_iteration_in_input(self.input)?
            .constrain_iteration_entailment()?
            .constrain_query_present(self.ptquery)?;
        Ok(())
    }
}

#[cfg(feature = "metrics_bench")]
impl<F: PrimeField> ConstraintSynthesizer<F> for ReasonerProof<F> {
    fn generate_constraints(
        mut self,
        cs: ConstraintSystemRef<F>,
    ) -> ark_relations::r1cs::Result<()> {
        AllocationGadget {}
            .allocate(&cs, &self.allocation_data, self.rules)?
            .constrain_first_iteration_in_input(self.input)?
            .constrain_iteration_entailment()?
            .constrain_query_present(self.ptquery)?;
        unsafe {
            *Rc::get_mut_unchecked(&mut self.metrics) = Metrics::from(&cs);
        }
        Ok(())
    }
}

impl<F: PrimeField> ReasonerProof<F> {
    /// Compare predicate and body of a fact.
    /// If either fact is false, the result will also be false.
    fn compare_facts(fact0: &ZKFact<F>, fact1: &ZKFact<F>) -> Result<Boolean<F>, SynthesisError> {
        // Compare arguments with a zip
        let arg_comparison = fact1
            .arguments
            .iter()
            .zip(fact0.arguments.iter())
            .flat_map(|(arg1, arg0)| vec![arg0.0.is_eq(&arg1.0), arg0.1.is_eq(&arg1.1)])
            .collect::<Result<Vec<Boolean<F>>, _>>()?;
        // Compare predicates
        Ok(std::iter::once(fact1.predicate.is_eq(&fact0.predicate)?)
            // Check validity
            .chain(std::iter::once(fact0.valid.clone()))
            .chain(std::iter::once(fact1.valid.clone()))
            .chain(arg_comparison.into_iter())
            .fold(Boolean::constant(true), |acc, b| acc & &b))
    }

    fn is_dummy_fact(fact: &ZKFact<F>) -> Result<Boolean<F>, SynthesisError> {
        let d = ZKFact::dummy(fact.arguments.len());
        Self::compare_facts(fact, &d)
    }
}

/// # Tests
#[cfg(test)]
mod tests {
    use super::*;
    use ark_bls12_377::{Bls12_377, Fr};
    use ark_groth16::Groth16;
    use ark_snark::{CircuitSpecificSetupSNARK, SNARK};
    use ark_std::rand::SeedableRng;

    use crate::utils::{tests::remove_witness_from_input, FromDatalog};
    use datalog::reasoning::{History, Iteration};

    impl<F: PrimeField> ZKFact<F> {
        /// This does *not* allocate!
        pub fn from_reasoner_assertion(fact: &Assertion<DATA>) -> Self {
            let arguments = fact
                .arguments
                .iter()
                .map(|f| {
                    let (t, d) = f.datafy();
                    (ZK_DATA::constant(t), ZK_DATA::constant(d))
                })
                .collect_vec();
            let valid_witness = true;
            let valid = Boolean::constant(true);
            let predicate = ZK_DATA::constant(fact.predicate);
            Self {
                valid_witness: Some(valid_witness),
                valid,
                predicate_witness: Some(fact.predicate),
                predicate,
                arguments_witness: Some(fact.arguments.iter().map(DataType::datafy).collect_vec()),
                arguments,
            }
        }
    }
    /// ## Test Fact Comparison
    #[test]
    fn comparison_test_equal_facts() {
        let f1 = ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, buzz)."));
        let f2 = ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, buzz)."));

        let res = ReasonerProof::compare_facts(&f1, &f2);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Boolean::constant(true));
    }
    #[test]
    fn comparison_test_neq_pred() {
        let f1 = ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, buzz)."));
        let f2 = ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("oof(bar, buzz)."));

        let res = ReasonerProof::compare_facts(&f1, &f2);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Boolean::constant(false));
    }

    #[test]
    fn comparison_test_neq_arg1() {
        let f1 = ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, buzz)."));
        let f2 =
            ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bork, buzz)."));

        let res = ReasonerProof::compare_facts(&f1, &f2);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Boolean::constant(false));
    }
    #[test]
    fn comparison_test_neq_arg2() {
        let f1 = ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, buzz)."));
        let f2 = ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, bork)."));

        let res = ReasonerProof::compare_facts(&f1, &f2);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Boolean::constant(false));
    }

    #[test]
    fn comparison_test_neq_type() {
        let mut f1 = ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar)."));
        f1.arguments[0].0 = ZK_DATA::constant(DATATYPE_NUMBER);
        let f2 = ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar)."));

        let res = ReasonerProof::compare_facts(&f1, &f2);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Boolean::constant(false));
    }

    /// ### Invalids
    #[test]
    fn comparison_test_inv_1() {
        let mut f1 =
            ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, buzz)."));
        f1.valid = Boolean::constant(false);
        let f2 = ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, buzz)."));

        let res = ReasonerProof::compare_facts(&f1, &f2);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Boolean::constant(false));
    }
    #[test]
    fn comparison_test_inv_2() {
        let f1 = ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, buzz)."));
        let mut f2 =
            ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, buzz)."));
        f2.valid = Boolean::constant(false);

        let res = ReasonerProof::compare_facts(&f1, &f2);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Boolean::constant(false));
    }
    #[test]
    fn comparison_test_both_2() {
        let mut f1 =
            ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, buzz)."));
        f1.valid = Boolean::constant(false);
        let mut f2 =
            ZKFact::<Fr>::from_reasoner_assertion(&Assertion::from_datalog("foo(bar, buzz)."));
        f2.valid = Boolean::constant(false);

        let res = ReasonerProof::compare_facts(&f1, &f2);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Boolean::constant(false));
    }

    /// ## Test Number comparison
    /// ### Greater than
    #[test]
    fn number_comparison_gt_prover_1() {
        let lhs = ZK_DATA::<Fr>::constant(5);
        let rhs = ZK_DATA::constant(4);
        let gt = greater_than(lhs, rhs);
        assert!(gt.is_ok());
        assert_eq!(gt.unwrap(), Boolean::constant(true));
    }
    #[test]
    fn number_comparison_gt_prover_2() {
        let lhs = ZK_DATA::<Fr>::constant(4);
        let rhs = ZK_DATA::constant(5);
        let gt = greater_than(lhs, rhs);
        assert!(gt.is_ok());
        assert_eq!(gt.unwrap(), Boolean::constant(false));
    }

    #[test]
    fn number_comparison_gt_prover_3() {
        let lhs = ZK_DATA::<Fr>::constant(4);
        let rhs = ZK_DATA::constant(4);
        let gt = greater_than(lhs, rhs);
        assert!(gt.is_ok());
        assert_eq!(gt.unwrap(), Boolean::constant(false));
    }

    /// ### Greater than or equals
    #[test]
    fn number_comparison_gte_prover_1() {
        let lhs = ZK_DATA::<Fr>::constant(5);
        let rhs = ZK_DATA::constant(4);
        let gte = greater_than_or_equal(lhs, rhs);
        assert!(gte.is_ok());
        assert_eq!(gte.unwrap(), Boolean::constant(true));
    }

    #[test]
    fn number_comparison_gte_prover_2() {
        let lhs = ZK_DATA::<Fr>::constant(4);
        let rhs = ZK_DATA::constant(5);
        let gte = greater_than_or_equal(lhs, rhs);
        assert!(gte.is_ok());
        assert_eq!(gte.unwrap(), Boolean::constant(false));
    }

    #[test]
    fn number_comparison_gte_prover_3() {
        let lhs = ZK_DATA::<Fr>::constant(4);
        let rhs = ZK_DATA::constant(4);
        let gte = greater_than_or_equal(lhs, rhs);
        assert!(gte.is_ok());
        assert_eq!(gte.unwrap(), Boolean::constant(true));
    }

    /// ### Allocation Test
    struct ZKFactAllocator<'a> {
        prover: bool,
        fact: &'a Assertion<DATA>,
        most_arguments: usize,
    }
    impl<'a, F: PrimeField> ConstraintSynthesizer<F> for ZKFactAllocator<'a> {
        fn generate_constraints(
            self,
            cs: ConstraintSystemRef<F>,
        ) -> ark_relations::r1cs::Result<()> {
            let f1 = if self.prover {
                ZKFact::allocate_from_reasoner_assertion(&cs, self.fact, self.most_arguments)
                    .unwrap()
            } else {
                ZKFact::allocate_without_witness(&cs, self.most_arguments).unwrap()
            };
            let f2 = ZKFact::from_reasoner_assertion_with_dummies(self.fact, 2);
            f1.valid.enforce_equal(&f2.valid).unwrap();
            f1.predicate.enforce_equal(&f2.predicate).unwrap();
            f1.arguments[0].0.enforce_equal(&f2.arguments[0].0).unwrap();
            f1.arguments[0].1.enforce_equal(&f2.arguments[0].1).unwrap();
            f1.arguments[1].0.enforce_equal(&f2.arguments[1].0).unwrap();
            f1.arguments[1].1.enforce_equal(&f2.arguments[1].1).unwrap();
            Ok(())
        }
    }
    #[test]
    fn allocate_fact() {
        let assertion = Assertion::from_datalog("foo(bar).");
        // Proof setup variables
        let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
        // Verifier
        let verifier = ZKFactAllocator {
            prover: false,
            fact: &assertion,
            most_arguments: 2,
        };
        // Prover
        let prover = ZKFactAllocator {
            prover: true,
            fact: &assertion,
            most_arguments: 2,
        };
        let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
        let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
        // Compare
        let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
        assert!(verif);
    }

    /// ## First iteration tests
    struct FirstIterationSynther<F: PrimeField> {
        allocation_data: AllocationData,
        rules: Ruleset<String, DATA>,
        input: InputData<F>,
    }
    impl<F: PrimeField> ConstraintSynthesizer<F> for FirstIterationSynther<F> {
        fn generate_constraints(
            self,
            cs: ConstraintSystemRef<F>,
        ) -> ark_relations::r1cs::Result<()> {
            let gadget = AllocationGadget {}.allocate(&cs, &self.allocation_data, self.rules)?;
            gadget
                .constrain_first_iteration_in_input(self.input)
                .unwrap();
            Ok(())
        }
    }
    /// ### Correct First Iteration
    #[test]
    fn first_iter_test_correct() {
        let fact0 = Assertion::from_datalog("hasFriend(alice, bob).");
        let fact1 = Assertion::from_datalog("hasWall(alice, wallA).");
        let facts = vec![fact0, fact1];
        let rules = Ruleset { rules: vec![] };
        let meta = IterationMetadata {
            iterations: 1,
            largest_assertion_set_size: 2,
            most_arguments: 2,
        };
        let history = History::new(
            vec![Iteration {
                assertions: facts.clone(),
            }],
            facts.clone(),
        );
        let input_data = InputData {
            arguments: 2,
            assertions: facts
                .iter()
                .map(ZKFact::<Fr>::from_reasoner_assertion)
                .collect(),
        };
        // Proof setup variables
        let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
        // Verifier
        let verifier = FirstIterationSynther {
            allocation_data: AllocationData::Metadata(meta),
            rules: rules.clone(),
            input: remove_witness_from_input(&input_data),
        };
        // Prover
        let prover = FirstIterationSynther {
            allocation_data: AllocationData::History(history),
            rules,
            input: input_data,
        };
        let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
        let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
        // Compare
        let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
        assert!(verif);
    }

    /// ### Incorrect First Iteration
    #[test]
    #[should_panic]
    fn first_iter_test_incorrect() {
        let fact0 = Assertion::from_datalog("hasFriend(alice, bob).");
        let fact1 = Assertion::from_datalog("hasWall(alice, wallA).");
        let fact_fake = Assertion::from_datalog("nop(foo, bar).");
        let facts = vec![fact0.clone(), fact1];
        let rules = Ruleset { rules: vec![] };
        let meta = IterationMetadata {
            iterations: 1,
            largest_assertion_set_size: 2,
            most_arguments: 2,
        };
        let history = History::new(
            vec![Iteration {
                assertions: vec![fact0, fact_fake.clone()],
            }],
            facts.clone(),
        );
        let input_data = InputData {
            arguments: 2,
            assertions: facts
                .iter()
                .map(ZKFact::<Fr>::from_reasoner_assertion)
                .collect(),
        };
        // Proof setup variables
        let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
        // Verifier
        let verifier = FirstIterationSynther {
            allocation_data: AllocationData::Metadata(meta),
            rules: rules.clone(),
            input: remove_witness_from_input(&input_data),
        };
        // Prover
        let prover = FirstIterationSynther {
            allocation_data: AllocationData::History(history),
            rules,
            input: input_data,
        };
        let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
        // Crashes below in test mode because constraints are not satisfied
        let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
        // Compare
        let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
        // Crashes below in release mode because verification fails
        assert!(verif);
    }

    /// ## Rule application validity Tests
    struct RuleApplSynth<F: PrimeField> {
        rules: Ruleset<String, DATA>,
        facts: Vec<ZKFact<F>>,
        result: ZKFact<F>,
    }
    impl<F: PrimeField> ConstraintSynthesizer<F> for RuleApplSynth<F> {
        fn generate_constraints(
            self,
            _cs: ConstraintSystemRef<F>,
        ) -> ark_relations::r1cs::Result<()> {
            let input = InputData {
                arguments: 0,
                assertions: vec![],
            };
            let iteration_data = IterationData {
                meta: IterationMetadata {
                    iterations: 0,
                    largest_assertion_set_size: 0,
                    most_arguments: 0,
                },
                assertions: vec![],
            };
            let gadget = IterationsGadget {
                input,
                iteration_data,
                rules: self.rules.clone(),
            };
            let res = gadget.apply_rule(&self.rules.rules[0], &self.facts)?;
            let eq = ReasonerProof::compare_facts(&res, &self.result)?;
            eq.enforce_equal(&Boolean::constant(true))?;
            Ok(())
        }
    }
    /// ### Argument matches
    #[test]
    fn rule_appl_valid_1_arg() {
        let source = Assertion::from_datalog("foo(bob).");
        let target = Assertion::from_datalog("bar(bob).");
        let rule = ReasonerRule::from_datalog("bar(Bob) :- foo(Bob).");
        let rules = Ruleset { rules: vec![rule] };
        // Proof setup variables
        let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
        // Verifier
        let verifier = RuleApplSynth {
            rules: rules.clone(),
            facts: vec![ZKFact::<Fr>::from_reasoner_assertion(&source)],
            result: ZKFact::<Fr>::from_reasoner_assertion(&target),
        };
        // Prover
        let prover = RuleApplSynth {
            rules: rules.clone(),
            facts: vec![ZKFact::<Fr>::from_reasoner_assertion(&source)],
            result: ZKFact::<Fr>::from_reasoner_assertion(&target),
        };
        let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
        let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
        // Compare
        let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
        assert!(verif);
    }

    /// ### Argument not matching
    #[should_panic]
    #[test]
    fn rule_appl_invalid_1_arg() {
        let source = Assertion::from_datalog("buzz(bob).");
        let target = Assertion::from_datalog("bar(bob).");
        let rule = ReasonerRule::from_datalog("bar(Bob) :- foo(Bob).");
        let rules = Ruleset { rules: vec![rule] };
        // Proof setup variables
        let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
        // Verifier
        let verifier = RuleApplSynth {
            rules: rules.clone(),
            facts: vec![ZKFact::<Fr>::from_reasoner_assertion(&source)],
            result: ZKFact::<Fr>::from_reasoner_assertion(&target),
        };
        // Prover
        let prover = RuleApplSynth {
            rules: rules.clone(),
            facts: vec![ZKFact::<Fr>::from_reasoner_assertion(&source)],
            result: ZKFact::<Fr>::from_reasoner_assertion(&target),
        };
        let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
        // Crashes below in test mode because constraints are not satisfied
        let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
        // Compare
        let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
        // Crashes below in release mode because verification fails
        assert!(verif);
    }

    /// ## Reasoning step validity Test
    struct ReasoningStepSynther<F: PrimeField> {
        input: InputData<F>,
        rules: Ruleset<String, DATA>,
        source: Vec<ZKFact<F>>,
        target: Vec<ZKFact<F>>,
    }
    impl<F: PrimeField> ConstraintSynthesizer<F> for ReasoningStepSynther<F> {
        fn generate_constraints(
            self,
            _cs: ConstraintSystemRef<F>,
        ) -> ark_relations::r1cs::Result<()> {
            let meta = IterationMetadata {
                iterations: 0,
                largest_assertion_set_size: 0,
                most_arguments: 0,
            };
            let iteration_data = IterationData {
                meta,
                assertions: vec![],
            };
            let gadget = IterationsGadget {
                input: self.input,
                iteration_data,
                rules: self.rules,
            };
            gadget.reasoner_iteration(&self.source, &self.target)?;
            Ok(())
        }
    }

    /// ### Valid Reasoning
    #[test]
    fn reasoning_step_valid_1_arg() {
        let source = Assertion::from_datalog("foo(bob).");
        let target = Assertion::from_datalog("bar(bob).");
        let rule = ReasonerRule::from_datalog("bar(Bob) :- foo(Bob).");
        let rules = Ruleset { rules: vec![rule] };
        let input = InputData {
            arguments: 1,
            assertions: vec![ZKFact::<Fr>::from_reasoner_assertion(&source)],
        };
        // Proof setup variables
        let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
        // Verifier
        let verifier = ReasoningStepSynther {
            input: remove_witness_from_input(&input),
            rules: rules.clone(),
            source: vec![ZKFact::<Fr>::from_reasoner_assertion(&source)],
            target: vec![ZKFact::<Fr>::from_reasoner_assertion(&target)],
        };
        // Prover
        let prover = ReasoningStepSynther {
            input,
            rules,
            source: vec![ZKFact::<Fr>::from_reasoner_assertion(&source)],
            target: vec![ZKFact::<Fr>::from_reasoner_assertion(&target)],
        };
        let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
        let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
        // Compare
        let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
        assert!(verif);
    }

    /// ### Invalid Reasoning
    #[test]
    #[should_panic]
    fn reasoning_step_invalid_1_arg() {
        let source = Assertion::from_datalog("buzz(bob).");
        let target = Assertion::from_datalog("bar(bob).");
        let rule = ReasonerRule::from_datalog("bar(Bob) :- foo(Bob).");
        let rules = Ruleset { rules: vec![rule] };
        let input = InputData {
            arguments: 1,
            assertions: vec![ZKFact::<Fr>::from_reasoner_assertion(&source)],
        };
        // Proof setup variables
        let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
        // Verifier
        let verifier = ReasoningStepSynther {
            input: remove_witness_from_input(&input),
            rules: rules.clone(),
            source: vec![ZKFact::<Fr>::from_reasoner_assertion(&source)],
            target: vec![ZKFact::<Fr>::from_reasoner_assertion(&target)],
        };
        // Prover
        let prover = ReasoningStepSynther {
            input,
            rules,
            source: vec![ZKFact::<Fr>::from_reasoner_assertion(&source)],
            target: vec![ZKFact::<Fr>::from_reasoner_assertion(&target)],
        };
        let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
        // Crashes below in test mode because constraints are not satisfied
        let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
        // Compare
        let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
        // Crashes below in release mode because verification fails
        assert!(verif);
    }
}
