use crate::{DATA, DATA_LENGTH};
use datalog::parsing::{
    Argument, ArithmeticSubgoal, Atom, Clause, Fact, LogicSubgoal, Parser, Question, Rule,
    RuleHead, Subgoal,
};
use datalog::reasoning::{Assertion, DataType, ReasonerRule, Ruleset};
use itertools::Itertools;

/// Hash a Number
fn hash(s: &str) -> DATA {
    use sha3::{Digest, Sha3_256};
    let mut h = Sha3_256::new();
    h.update(s);
    let bytes: [u8; DATA_LENGTH] = h.finalize().into();
    let mut bytes_trunc = [0u8; 4];
    bytes_trunc.copy_from_slice(&bytes[0..4]);
    u32::from_le_bytes(bytes_trunc)
}

fn hash_atom(atm: &Atom<String>) -> Atom<DATA> {
    Atom {
        name: hash(&atm.name),
    }
}

/// String or Datalog interpreter to a Circuitree usable type
pub trait FromDatalog<T> {
    fn from_datalog(datalog: T) -> Self;
}

impl FromDatalog<Argument<String, String>> for Argument<String, DATA> {
    fn from_datalog(datalog: Argument<String, String>) -> Self {
        match datalog {
            Argument::Atom(atm) => {
                let new_atm = hash_atom(&atm);
                Argument::Atom(new_atm)
            }
            Argument::Number(nr) => Argument::Number(nr),
            Argument::Variable(var) => Argument::Variable(var),
        }
    }
}

impl FromDatalog<ArithmeticSubgoal<String, String>> for ArithmeticSubgoal<String, DATA> {
    fn from_datalog(datalog: ArithmeticSubgoal<String, String>) -> Self {
        // We assume a correct rule, so never two Numbers
        let lhs = Argument::from_datalog(datalog.lhs);
        let rhs = Argument::from_datalog(datalog.rhs);
        ArithmeticSubgoal {
            lhs,
            operator: datalog.operator,
            rhs,
        }
    }
}

impl FromDatalog<Rule<String, String>> for ReasonerRule<String, DATA> {
    fn from_datalog(datalog: Rule<String, String>) -> Self {
        let head = RuleHead {
            predicate: hash(&datalog.head.predicate),
            arguments: datalog
                .head
                .arguments
                .iter()
                .map(|v| match v {
                    Argument::Atom(atm) => Argument::Atom(hash_atom(atm)),
                    Argument::Variable(var) => Argument::Variable(var.clone()),
                    Argument::Number(_) => unimplemented!("Arithmetic reasoning in rule head"),
                })
                .collect_vec(),
        };
        let subgoals = datalog
            .subgoals
            .into_iter()
            .map(|sg| match sg {
                Subgoal::Logic(lsg) => Subgoal::Logic(LogicSubgoal {
                    predicate: hash(&lsg.predicate),
                    arguments: lsg
                        .arguments
                        .iter()
                        .map(|v| match v {
                            Argument::Atom(atm) => Argument::Atom(hash_atom(atm)),
                            Argument::Variable(var) => Argument::Variable(var.clone()),
                            Argument::Number(x) => Argument::Number(*x),
                        })
                        .collect_vec(),
                }),
                Subgoal::Arithmetic(a) => Subgoal::Arithmetic(ArithmeticSubgoal::from_datalog(a)),
            })
            .collect_vec();
        ReasonerRule::new(&head, &subgoals)
    }
}

impl FromDatalog<&'_ str> for ReasonerRule<String, DATA> {
    fn from_datalog(datalog: &'_ str) -> Self {
        if let Ok(Clause::Rule(r)) = Parser::parse(datalog) {
            ReasonerRule::from_datalog(r)
        } else {
            panic!("Could not parse rule: {}", datalog);
        }
    }
}

impl FromDatalog<&'_ str> for Ruleset<String, DATA> {
    fn from_datalog(datalog: &'_ str) -> Self {
        Ruleset {
            rules: Parser::parse_ruleset(datalog)
                .unwrap()
                .into_iter()
                .map(ReasonerRule::from_datalog)
                .collect_vec(),
        }
    }
}

impl FromDatalog<Fact<String, String>> for Assertion<DATA> {
    fn from_datalog(datalog: Fact<String, String>) -> Self {
        Assertion {
            parents: None,
            predicate: hash(&datalog.predicate),
            arguments: datalog
                .arguments
                .into_iter()
                .map(|v| match v {
                    Argument::Atom(atm) => DataType::Atom(hash_atom(&atm)),
                    Argument::Variable(_) => panic!("Variable in Fact"),
                    Argument::Number(n) => DataType::Number(n),
                })
                .collect_vec(),
        }
    }
}

impl FromDatalog<&'_ str> for Assertion<DATA> {
    fn from_datalog(datalog: &'_ str) -> Self {
        if let Ok(Clause::Fact(f)) = Parser::parse(datalog) {
            Assertion::from_datalog(f)
        } else {
            panic!("Could not parse assertion: {}", datalog);
        }
    }
}

impl FromDatalog<&'_ str> for Vec<Assertion<DATA>> {
    fn from_datalog(datalog: &'_ str) -> Self {
        Parser::parse_assertionset(datalog)
            .unwrap()
            .into_iter()
            .map(Assertion::from_datalog)
            .collect_vec()
    }
}

impl FromDatalog<&'_ str> for Question<String, DATA> {
    fn from_datalog(datalog: &'_ str) -> Question<String, u32> {
        let question = match Parser::parse(datalog).unwrap() {
            Clause::Question(q) => q,
            _ => panic!("Error: query not parsed as question"),
        };

        Question {
            predicate: hash(&question.predicate),
            arguments: question
                .arguments
                .into_iter()
                .map(|v| {
                    match v {
                        Argument::Atom(atm) => Argument::Atom(Atom {
                            name: hash(&atm.name),
                        }),
                        Argument::Variable(_) => {
                            unimplemented!("Variable in question not implemented")
                        } //Should it be implemented?
                        Argument::Number(_) => unimplemented!(),
                    }
                })
                .collect_vec(),
        }
    }
}

/// #Helpers
pub mod tests {
    use crate::{InputData, ZKFact};
    use ark_ff::fields::PrimeField;
    pub fn remove_witness_from_input<F: PrimeField>(id: &InputData<F>) -> InputData<F> {
        let assertions = id
            .assertions
            .iter()
            .map(|a| {
                ZKFact::new_without_witness(
                    a.valid.clone(),
                    a.predicate.clone(),
                    a.arguments.clone(),
                )
            })
            .collect();
        InputData {
            arguments: id.arguments,
            assertions,
        }
    }
}

/// # Metrics
#[cfg(feature = "metrics_bench")]
pub mod metrics {
    use ark_ff::Field;
    use ark_relations::r1cs::ConstraintSystemRef;
    use std::fmt;

    #[derive(Default, Clone)]
    pub struct Metrics {
        pub constraints: usize,
        pub instance_variables: usize,
        pub witness_variables: usize,
    }

    impl<F: Field> From<&ConstraintSystemRef<F>> for Metrics {
        fn from(cs: &ConstraintSystemRef<F>) -> Self {
            Self {
                constraints: cs.num_constraints(),
                instance_variables: cs.num_instance_variables(),
                witness_variables: cs.num_witness_variables(),
            }
        }
    }

    impl fmt::Display for Metrics {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(
                f,
                "{{constraints: {}, instance_variables: {}, witness_variables: {}}}",
                self.constraints, self.instance_variables, self.witness_variables
            )
        }
    }
}
