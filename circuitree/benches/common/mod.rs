use ark_ec::pairing::Pairing;
use ark_groth16::{Groth16, ProvingKey, VerifyingKey};
use ark_snark::{CircuitSpecificSetupSNARK, SNARK};
use ark_std::rand::prelude::StdRng;
use circuitree::utils::tests::remove_witness_from_input;
use circuitree::utils::*;
use circuitree::{
    get_max_arguments, AllocationData, InputData, IterationMetadata, ReasonerProof, DATA,
};
use datalog::parsing::Question;
use datalog::reasoning::{Assertion, History, Reasoner, ReasonerRule, Ruleset};
use itertools::Itertools;

#[cfg(not(feature = "metrics_bench"))]
use {ark_groth16::Proof, ark_relations::r1cs::SynthesisError};

#[cfg(feature = "metrics_bench")]
use {circuitree::utils::metrics::Metrics, std::rc::Rc};

pub fn generate_dataset(
    string_rules: Vec<String>,
    string_facts: Vec<String>,
    query_string: String,
) -> (
    Ruleset<String, DATA>,
    Vec<Assertion<DATA>>,
    Question<String, DATA>,
) {
    let rules = Ruleset {
        rules: string_rules
            .into_iter()
            .map(|s| ReasonerRule::from_datalog(s.as_str()))
            .collect_vec(),
    };
    let facts = string_facts
        .into_iter()
        .map(|s| Assertion::from_datalog(s.as_str()))
        .collect_vec();

    let query = Question::from_datalog(&query_string);
    (rules, facts, query)
}

/// Test with assertion set size 1, varrying the number of iterations
pub fn linear_dataset(
    iterations: usize,
    max_iterations: usize,
) -> (
    Ruleset<String, DATA>,
    Vec<Assertion<DATA>>,
    Question<String, DATA>,
) {
    //Create the dataset
    let rules = (0..max_iterations as u8)
        .map(|n| {
            let c = b'A' + n;
            format!(
                "pred{}(ArgA):- pred{}(ArgA).",
                char::from(c + 1),
                char::from(c)
            )
        })
        .collect_vec();
    let facts = vec!["predA(x).".to_string()];
    let query_string = format!("?- pred{}(x).", char::from(b'A' + iterations as u8));
    generate_dataset(rules, facts, query_string)
}

pub struct Prover<PS: Pairing> {
    rules: Ruleset<String, DATA>,
    facts: Vec<Assertion<DATA>>,
    query: Question<String, DATA>,
    history: Option<History<DATA>>,
    input: Option<InputData<PS::ScalarField>>,
    proving_key: Option<ProvingKey<PS>>,
}

impl<PS: Pairing> Prover<PS> {
    pub fn new(
        rules: Ruleset<String, DATA>,
        facts: Vec<Assertion<DATA>>,
        query: Question<String, DATA>,
    ) -> Self {
        Self {
            rules,
            facts,
            query,
            history: None,
            input: None,
            proving_key: None,
        }
    }

    pub fn setup(&mut self, v: &mut Verifier<PS>, rng: &mut StdRng) {
        let mut preliminary_reasoner = Reasoner::default();
        // Add rules to reasoner
        preliminary_reasoner.add_ruleset(self.rules.clone());
        // Add data to reasoner
        preliminary_reasoner.add_assertion_set(self.facts.clone());
        // Generate query
        // Reason
        let x = preliminary_reasoner.query(&self.query).unwrap();
        // Get history
        // Create arguments for proof
        let history = x.get_history();
        let max_args = get_max_arguments(&history, &self.rules);
        let input = InputData::from_history(&history, max_args);

        // Create Verifier input
        let verifier_input = remove_witness_from_input(&input);
        let meta = IterationMetadata::from_history(&history, &self.rules);

        // Assign input and history
        self.history = Some(history);
        self.input = Some(input);

        self.proving_key = Some(v.setup(meta, verifier_input, rng));
    }

    #[cfg(not(feature = "metrics_bench"))]
    pub fn prove(self, rng: &mut StdRng) -> Proof<PS> {
        let prover = ReasonerProof {
            input: self.input.unwrap(),
            ptquery: self.query,
            rules: self.rules,
            allocation_data: AllocationData::History(self.history.unwrap()),
        };
        Groth16::<PS>::prove(&self.proving_key.unwrap(), prover, rng).unwrap()
    }

    #[cfg(feature = "metrics_bench")]
    pub fn prove_with_metrics(self, rng: &mut StdRng) -> Metrics {
        let metrics = Rc::new(Metrics::default());
        let prover = ReasonerProof {
            input: self.input.unwrap(),
            ptquery: self.query,
            rules: self.rules,
            allocation_data: AllocationData::History(self.history.unwrap()),
            metrics: metrics.clone(),
        };
        Groth16::<PS>::prove(&self.proving_key.unwrap(), prover, rng).unwrap();
        (*metrics).clone()
    }
}

#[cfg_attr(feature = "metrics_bench", allow(dead_code))]
pub struct Verifier<PS: Pairing> {
    rules: Ruleset<String, DATA>,
    query: Question<String, DATA>,
    vk: Option<VerifyingKey<PS>>,
}
impl<PS: Pairing> Verifier<PS> {
    pub fn new(rules: Ruleset<String, DATA>, query: Question<String, DATA>) -> Self {
        Self {
            rules,
            query,
            vk: None,
        }
    }
    pub fn setup(
        &mut self,
        meta: IterationMetadata,
        input: InputData<PS::ScalarField>,
        rng: &mut StdRng,
    ) -> ProvingKey<PS> {
        let v = ReasonerProof {
            input,
            ptquery: self.query.clone(),
            rules: self.rules.clone(),
            allocation_data: AllocationData::Metadata(meta),
            #[cfg(feature = "metrics_bench")]
            metrics: Rc::new(Metrics::default()),
        };
        let (pk, vk) = Groth16::<PS>::setup(v, rng).unwrap();
        self.vk = Some(vk);
        pk
    }

    #[cfg(not(feature = "metrics_bench"))]
    pub fn verify(&self, proof: Proof<PS>) -> Result<bool, SynthesisError> {
        Groth16::<PS>::verify(&self.vk.as_ref().unwrap(), &[], &proof)
    }
}

pub fn treelike_dataset_auto(
    i: usize,
) -> (
    Ruleset<String, DATA>,
    Vec<Assertion<DATA>>,
    Question<String, DATA>,
) {
    let elements = (2 << i) - 1;
    let leaves = (elements / 2..elements)
        .map(|i| format!("pred{}(a).", i))
        .collect_vec();
    let rules = (0..elements / 2)
        .map(|i| {
            format!(
                "pred{}(a) :- pred{}(a), pred{}(a).",
                i,
                i * 2 + 1,
                i * 2 + 2
            )
        })
        .collect_vec();
    generate_dataset(rules, leaves, "?- pred0(a).".to_string())
}

#[allow(dead_code)]
///Generate irrelevant rules
pub fn irrelevant_rules(amount: usize, subgoals: usize) -> Ruleset<String, DATA> {
    Ruleset {
        rules: (0..amount)
            .map(|n| {
                let subgoal = format!("iRul{}(iItem{})", n, n);
                let rhs_string = (0..subgoals - 1)
                    .fold(subgoal.clone(), |acc, _n| format!("{}, {}", acc, &subgoal));
                let string_rule = format!("{} :- {}.", &subgoal, rhs_string);
                ReasonerRule::from_datalog(string_rule.as_str())
            })
            .collect_vec(),
    }
}
