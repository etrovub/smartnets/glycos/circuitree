#![cfg(not(feature = "metrics_bench"))]
mod common;

use ark_bls12_377::Bls12_377;
use ark_ec::pairing::Pairing;
use ark_std::rand::SeedableRng;
use circuitree::utils::*;
use circuitree::DATA;
use common::*;
use criterion::measurement::WallTime;
use criterion::{
    criterion_group, criterion_main, BatchSize, BenchmarkGroup, BenchmarkId, Criterion,
};
use datalog::parsing::Question;
use datalog::reasoning::{Assertion, Ruleset};

type Curve = Bls12_377;

fn benchmark_setup<PS: Pairing>(
    group: &mut BenchmarkGroup<WallTime>,
    identifier: BenchmarkId,
    rules: Ruleset<String, DATA>,
    query: Question<String, DATA>,
    facts: Vec<Assertion<DATA>>,
) {
    group.bench_function(identifier, |b| {
        b.iter(|| {
            let mut rng = ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
            let mut prover = Prover::<PS>::new(rules.clone(), facts.clone(), query.clone());
            let mut verifier = Verifier::new(rules.clone(), query.clone());
            prover.setup(&mut verifier, &mut rng);
        });
    });
}

fn benchmark_prove<PS: Pairing>(
    group: &mut BenchmarkGroup<WallTime>,
    identifier: BenchmarkId,
    rules: Ruleset<String, DATA>,
    query: Question<String, DATA>,
    facts: Vec<Assertion<DATA>>,
) {
    group.bench_function(identifier, |b| {
        b.iter_batched(
            || {
                let mut rng = ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
                let mut prover = Prover::<PS>::new(rules.clone(), facts.clone(), query.clone());
                let mut verifier = Verifier::new(rules.clone(), query.clone());
                prover.setup(&mut verifier, &mut rng);
                (prover, rng)
            },
            |(prover, mut rng)| prover.prove(&mut rng),
            BatchSize::SmallInput,
        );
    });
}

fn benchmark_verify<PS: Pairing>(
    group: &mut BenchmarkGroup<WallTime>,
    identifier: BenchmarkId,
    rules: Ruleset<String, DATA>,
    query: Question<String, DATA>,
    facts: Vec<Assertion<DATA>>,
) {
    group.bench_function(identifier, |b| {
        b.iter_batched(
            || {
                let mut rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
                let mut prover = Prover::new(rules.clone(), facts.clone(), query.clone());
                let mut verifier = Verifier::<PS>::new(rules.clone(), query.clone());
                prover.setup(&mut verifier, &mut rng);
                let proof = prover.prove(&mut rng);
                (verifier, proof)
            },
            |(verifier, proof)| verifier.verify(proof),
            BatchSize::SmallInput,
        );
    });
}

fn linear_setup_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("linear_setup_iterations");
    let max_iterations = 6;
    for i in 0..max_iterations {
        let (rules, facts, query) = linear_dataset(i, max_iterations);
        benchmark_setup::<Curve>(
            &mut group,
            BenchmarkId::from_parameter(i),
            rules,
            query,
            facts,
        )
    }
}

fn linear_prove_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("linear_prove_iterations");
    let max_iterations = 6;
    for i in 0..max_iterations {
        let (rules, facts, query) = linear_dataset(i, max_iterations);
        benchmark_prove::<Curve>(
            &mut group,
            BenchmarkId::from_parameter(i),
            rules,
            query,
            facts,
        )
    }
}

fn linear_verify_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("linear_verify_iterations");
    let max_iterations = 6;
    for i in 0..max_iterations {
        let (rules, facts, query) = linear_dataset(i, max_iterations);
        benchmark_verify::<Curve>(
            &mut group,
            BenchmarkId::from_parameter(i),
            rules,
            query,
            facts,
        )
    }
}

fn treelike_setup_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("treelike_setup_iterations");
    let max_iterations = 6;
    for i in 0..max_iterations {
        let (rules, facts, query) = treelike_dataset_auto(i);
        benchmark_setup::<Curve>(
            &mut group,
            BenchmarkId::from_parameter(i),
            rules,
            query,
            facts,
        )
    }
}

fn treelike_prove_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("treelike_prove_iterations");
    let max_iterations = 6;
    for i in 0..max_iterations {
        let (rules, facts, query) = treelike_dataset_auto(i);
        benchmark_prove::<Curve>(
            &mut group,
            BenchmarkId::from_parameter(i),
            rules,
            query,
            facts,
        )
    }
}

fn treelike_verify_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("treelike_verify_iterations");
    let max_iterations = 6;
    for i in 0..max_iterations {
        let (rules, facts, query) = treelike_dataset_auto(i);
        benchmark_verify::<Curve>(
            &mut group,
            BenchmarkId::from_parameter(i),
            rules,
            query,
            facts,
        )
    }
}

const RULES_STR: &str = include_str!("../examples/cst/cst.datalog");
const FACTS_STR: &str = include_str!("../examples/cst/alice_cst.datalog");
const QUERY: &str = "?- ok(alice).";

fn cst_setup_bench(c: &mut Criterion) {
    let rules = Ruleset::from_datalog(RULES_STR);
    let facts = <Vec<_>>::from_datalog(FACTS_STR);
    let query = Question::from_datalog(QUERY);
    let mut group = c.benchmark_group("cst_iterations");
    benchmark_setup::<Curve>(
        &mut group,
        BenchmarkId::from_parameter("setup"),
        rules,
        query,
        facts,
    )
}

fn cst_prove_bench(c: &mut Criterion) {
    let rules = Ruleset::from_datalog(RULES_STR);
    let facts = <Vec<_>>::from_datalog(FACTS_STR);
    let query = Question::from_datalog(QUERY);
    let mut group = c.benchmark_group("cst_iterations");
    benchmark_prove::<Curve>(
        &mut group,
        BenchmarkId::from_parameter("prove"),
        rules,
        query,
        facts,
    )
}

fn cst_verify_bench(c: &mut Criterion) {
    let rules = Ruleset::from_datalog(RULES_STR);
    let facts = <Vec<_>>::from_datalog(FACTS_STR);
    let query = Question::from_datalog(QUERY);
    let mut group = c.benchmark_group("cst_iterations");
    benchmark_verify::<Curve>(
        &mut group,
        BenchmarkId::from_parameter("verify"),
        rules,
        query,
        facts,
    )
}

criterion_group!(
    benches,
    linear_setup_bench,
    linear_prove_bench,
    linear_verify_bench,
    treelike_setup_bench,
    treelike_prove_bench,
    treelike_verify_bench,
    cst_setup_bench,
    cst_prove_bench,
    cst_verify_bench,
);
criterion_main!(benches);
