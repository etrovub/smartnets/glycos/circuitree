#![cfg(feature = "metrics_bench")]
mod common;

use ark_bls12_377::Bls12_377;
use ark_ec::pairing::Pairing;
use ark_std::rand::SeedableRng;
use circuitree::utils::metrics::Metrics;
use circuitree::utils::FromDatalog;
use circuitree::DATA;
use common::*;
use datalog::parsing::Question;
use datalog::reasoning::{Assertion, Ruleset};

type Curve = Bls12_377;

fn get_metrics<PS: Pairing>(
    rules: Ruleset<String, DATA>,
    query: Question<String, DATA>,
    facts: Vec<Assertion<DATA>>,
) -> Metrics {
    let mut rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    let mut prover = Prover::<PS>::new(rules.clone(), facts.clone(), query.clone());
    let mut verifier = Verifier::new(rules.clone(), query.clone());
    prover.setup(&mut verifier, &mut rng);
    prover.prove_with_metrics(&mut rng)
}

fn linear_bench() {
    println!("# Linear, varriable iterations");
    let max_iterations = 6;
    for i in 0..max_iterations {
        let (rules, facts, query) = linear_dataset(i, max_iterations);
        println!("{}, {}", i, get_metrics::<Curve>(rules, query, facts));
    }
}

fn treelike_bench() {
    println!("# Treelike, varriable iterations");
    let max_iterations = 6;
    for i in 0..max_iterations {
        let (rules, facts, query) = treelike_dataset_auto(i);
        println!("{}, {}", i, get_metrics::<Curve>(rules, query, facts));
    }
}

const RULES_STR: &str = include_str!("../examples/cst/cst.datalog");
const FACTS_STR: &str = include_str!("../examples/cst/alice_cst.datalog");
const QUERY: &str = "?- ok(alice).";

fn cst_bench() {
    println!("# CST");
    let rules = Ruleset::from_datalog(RULES_STR);
    let facts = <Vec<_>>::from_datalog(FACTS_STR);
    let query = Question::from_datalog(QUERY);
    println!("{}", get_metrics::<Curve>(rules, query, facts));
}

fn main() {
    linear_bench();
    treelike_bench();
    cst_bench();
}
