use ark_bls12_377::{Bls12_377, Fr};
use ark_ff::fields::PrimeField;
use ark_groth16::Groth16;
use ark_snark::{CircuitSpecificSetupSNARK, SNARK};
use ark_std::rand::SeedableRng;
// The field type for when it's not derived
/// # Reasoning tests
use circuitree::utils::{tests::remove_witness_from_input, FromDatalog};
use circuitree::*;
use datalog::parsing::{Data, Question};
use datalog::reasoning::*;
use itertools::Itertools;

/// ## Test Dataset 1
/// A dataset containing 3 facts and 1 rule:
/// - hasFriend(alice, bob).
/// - hasFriend(bob, carol).
/// - hasWall(alice, wallA).
///
/// - canWrite(Bob, WallA) :- hasFriend(Alice, Bob), hasWall(Alice, WallA).
///
/// From this dataset can be derived:
/// - canWrite(bob, wallA).
pub fn initial_dataset<F: PrimeField>(
    query_string: &str,
) -> (
    History<DATA>,
    InputData<F>,
    Question<String, DATA>,
    Ruleset<String, DATA>,
) {
    let facts = vec![
        "hasFriend(alice, bob).",
        "hasFriend(bob, carol).",
        "hasWall(alice, wallA).",
    ];
    let facts = facts.into_iter().map(Assertion::from_datalog);
    let rules = Ruleset::from_datalog(
        "canWrite(Bob, WallA) :- hasFriend(Alice, Bob), hasWall(Alice, WallA).",
    );

    let mut preliminary_reasoner = Reasoner::default();
    facts.for_each(|f| preliminary_reasoner.add_fact(f));
    rules
        .iter()
        .for_each(|r| preliminary_reasoner.add_rule(r.clone()));

    let query = Question::from_datalog(query_string);
    let query_res = preliminary_reasoner.query(&query).unwrap();
    let history = query_res.get_history();
    let max_args = get_max_arguments(&history, &rules);
    let id = InputData::from_history(&history, max_args);
    (history, id, query, rules)
}

/// A dataset containing 3 facts and no rules:
/// hasType(apple, fruit).
/// hasType(bob, person).
/// hasRating(apple, good).
fn initial_dataset_2<F: PrimeField>(
    query_string: &str,
) -> (
    History<DATA>,
    InputData<F>,
    Question<String, DATA>,
    Ruleset<String, DATA>,
) {
    let facts = vec![
        "hasType(apple, fruit).",
        "hasType(bob, person).",
        "hasRating(apple, good).",
    ];
    let facts = facts.into_iter().map(Assertion::from_datalog);
    let rules = Ruleset { rules: vec![] };

    let mut preliminary_reasoner = Reasoner::default();
    facts.for_each(|f| preliminary_reasoner.add_fact(f));

    let query = Question::from_datalog(query_string);
    let query_res = preliminary_reasoner.query(&query).unwrap();
    let history = query_res.get_history();
    let max_args = get_max_arguments(&history, &rules);
    let id = InputData::from_history(&history, max_args);
    (history, id, query, rules)
}

/// ## "Realistic" Dataset
/// A dataset containg 4 facts and 3 rules:
/// - hasFriend(alice, bob).
/// - hasWall(alice, aliceWall).
/// - hasAdmin(group, bob).
/// - hasWall(group, groupWall).
///
/// - canWrite(PersonB, WallA) :- hasFriend(PersonA, PersonB), hasWall(PersonA, WallA).
/// - canWrite(PersonA, WallA) :- hasMember(GroupA, PersonA), hasWall(GroupA, WallA).
/// - hasMember(Group, Person) :- hasAdmin(Group, Person).
///
/// From this dataset can be derived:
/// - canWrite(bob, alicewall).
/// - hasMember(group, bob).
/// - canWrite(bob, groupWall).
fn realistic_dataset<F: PrimeField>(
    query_string: &str,
) -> (
    History<DATA>,
    InputData<F>,
    Question<String, DATA>,
    Ruleset<String, DATA>,
) {
    let rules = "canWrite(PersonB, WallA) :- hasFriend(PersonA, PersonB), hasWall(PersonA, WallA).
        canWrite(PersonA, WallA) :- hasMember(GroupA, PersonA), hasWall(GroupA, WallA).
        hasMember(Group, Person) :- hasAdmin(Group, Person).";
    let rules = Ruleset::from_datalog(rules);
    let facts = vec![
        "hasFriend(alice, bob).",
        "hasWall(alice, aliceWall).",
        "hasAdmin(group, bob).",
        "hasWall(group, groupWall).",
    ];
    let facts = facts
        .into_iter()
        .map(|s| Assertion::from_datalog(s))
        .collect_vec();
    let mut preliminary_reasoner = Reasoner::default();
    facts
        .into_iter()
        .for_each(|f| preliminary_reasoner.add_fact(f));
    rules
        .iter()
        .for_each(|r| preliminary_reasoner.add_rule(r.clone()));
    let query = Question::from_datalog(query_string);
    let query_res = preliminary_reasoner.query(&query).unwrap();
    let history = query_res.get_history();
    let max_args = get_max_arguments(&history, &rules);
    let id = InputData::from_history(&history, max_args);
    (history, id, query, rules)
}

/// ## Erronous Test Dataset 1
/// A Dataset whose last iteration is removed from the history.
/// The Dataset contains 3 facts and 1 rule:
/// - hasFriend(alice, bob).
/// - hasFriend(bob, carol).
/// - hasWall(alice, wallA).
///
/// - canWrite(Bob, WallA) :- hasFriend(Alice, Bob), hasWall(Alice, WallA).
fn dataset_missing_last_iteration<F: PrimeField>(
    query_string: &str,
) -> (
    History<DATA>,
    InputData<F>,
    Question<String, DATA>,
    Ruleset<String, DATA>,
) {
    let facts = vec![
        //"Person(Alice).",
        //"Person(Bob)",
        //"Person(Carol)",
        "hasFriend(alice, bob).",
        "hasFriend(bob, carol).",
        "hasWall(alice, wallA).",
    ];
    let facts = facts.into_iter().map(Assertion::from_datalog);
    let rules = "canWrite(Bob, WallA) :- hasFriend(Alice, Bob), hasWall(Alice, WallA).";
    let rules = Ruleset::from_datalog(rules);
    let mut preliminary_reasoner = Reasoner::default();
    facts.for_each(|f| preliminary_reasoner.add_fact(f));
    rules
        .iter()
        .for_each(|r| preliminary_reasoner.add_rule(r.clone()));

    let query = Question::from_datalog(query_string);
    let query_res = preliminary_reasoner.query(&query).unwrap();
    let history = query_res.get_history();
    let max_args = get_max_arguments(&history, &rules);
    let mut faked_history = history;
    faked_history.iterations.pop();
    let id = InputData::from_history(&faked_history, max_args);
    (faked_history, id, query, rules)
}

/// ## Two Step Dataset
/// A dataset for two iterations containing 2 rules and 2 facts:
/// - predA(a).
/// - predB(a).
///
/// - predC(X) :- predA(X).
/// - predD(X) :- predC(X), predB(X).
///
/// The dataset queries for:
/// - ?- predD(a).
fn two_step_dataset<F: PrimeField>() -> (
    History<DATA>,
    InputData<F>,
    Question<String, DATA>,
    Ruleset<String, DATA>,
) {
    let facts = <Vec<Assertion<DATA>>>::from_datalog("predA(a).\npredB(a).");
    let rules = "predC(X) :- predA(X).
    predD(X) :- predC(X), predB(X).";
    let rules = Ruleset::from_datalog(rules);
    let mut preliminary_reasoner = Reasoner::default();
    facts
        .into_iter()
        .for_each(|f| preliminary_reasoner.add_fact(f));
    rules
        .iter()
        .for_each(|r| preliminary_reasoner.add_rule(r.clone()));

    let query_string = "?- predD(a).";
    let query = Question::from_datalog(query_string);
    let query_res = preliminary_reasoner.query(&query).unwrap();
    let history = query_res.get_history();
    let max_args = get_max_arguments(&history, &rules);
    let id = InputData::from_history(&history, max_args);
    (history, id, query, rules)
}

/// ## Treelike Dataset
/// Generates a treelike dataset with the root the result.
/// Queries the result.
fn treelike_dataset_auto<F: PrimeField>(
    i: usize,
) -> (
    History<DATA>,
    InputData<F>,
    Question<String, DATA>,
    Ruleset<String, DATA>,
) {
    let elements = (2 << i) - 1;
    let leaves = (elements / 2..elements)
        .map(|i| Assertion::from_datalog(&*format!("pred{}(a).", i)))
        .collect_vec();
    let rules = Ruleset {
        rules: (0..elements / 2)
            .map(|i| {
                ReasonerRule::from_datalog(&*format!(
                    "pred{}(a) :- pred{}(a), pred{}(a).",
                    i,
                    i * 2 + 1,
                    i * 2 + 2
                ))
            })
            .collect_vec(),
    };
    let mut preliminary_reasoner = Reasoner::default();
    leaves
        .into_iter()
        .for_each(|f| preliminary_reasoner.add_fact(f));
    rules
        .iter()
        .for_each(|r| preliminary_reasoner.add_rule(r.clone()));

    let query = Question::from_datalog("?- pred0(a).");
    let query_res = preliminary_reasoner.query(&query).unwrap();
    let history = query_res.get_history();
    let max_args = get_max_arguments(&history, &rules);
    let id = InputData::from_history(&history, max_args);
    (history, id, query, rules)
}

/// ## Dataset with invalid reasoning in history
/// This test uses the following dataset and query:
/// ```prolog
/// bar(bob) :- foo(bob).
/// buzz(bob).
/// ?- bar(bob).
/// ```
/// If the valid flag works, this test should fail because buzz != foo.
///
/// Note: we have to give a parent to the resulting fact, otherwise
/// it will be included in the assertion sets!
/// This is **not** an issue with the reasoner, but for a different gadget
/// which has to see if supplied data actually exists (e.g. signature verification).
fn invalid_test_set<F: PrimeField>() -> (
    History<DATA>,
    InputData<F>,
    Question<String, DATA>,
    Ruleset<String, DATA>,
) {
    use std::rc::Rc;
    let fact_1 = Assertion::from_datalog("buzz(bob).");
    let rules = "bar(Bob) :- foo(Bob).";
    let rules = Ruleset::from_datalog(rules);
    let mut res_fact = Assertion::from_datalog("bar(bob).");
    res_fact.parents = Some(vec![Rc::new(fact_1.clone())]);
    let query_string = "?- bar(bob).";
    let query = Question::from_datalog(query_string);
    let i1 = Iteration {
        assertions: vec![fact_1.clone()],
    };
    let i2 = Iteration {
        assertions: vec![res_fact],
    };
    let history = History::new(vec![i1, i2], vec![fact_1]);
    let max_args = get_max_arguments(&history, &rules);
    let id = InputData::from_history(&history, max_args);
    (history, id, query, rules)
}
/// ## Dataset with simple arithmetic and forged history:
/// ```prolog
/// hasAge(alice, 16).
/// isAdult(Alice) :- hasAge(Alice, Age), Age >= 18.
fn simple_arithmetic_dataset_wrong<F: PrimeField>() -> (
    History<DATA>,
    InputData<F>,
    Question<String, DATA>,
    Ruleset<String, DATA>,
) {
    let fact = Assertion::from_datalog("hasAge(alice, 20).");
    let faked_fact = Assertion::from_datalog("hasAge(alice, 16).");
    let rule = ReasonerRule::from_datalog("isAdult(Alice) :- hasAge(Alice, Age), Age >= 18.");
    let rules = Ruleset {
        rules: vec![rule.clone()],
    };

    let mut preliminary_reasoner = Reasoner::default();
    preliminary_reasoner.add_fact(fact.clone());
    preliminary_reasoner.add_rule(rule);
    let query = Question::from_datalog("?- isAdult(alice).");
    let query_res = preliminary_reasoner.query(&query).unwrap();
    let history = query_res.get_history();
    let max_args = get_max_arguments(&history, &rules);
    let faked_iterations = history
        .iterations
        .into_iter()
        .map(|i| Iteration {
            assertions: i
                .assertions
                .into_iter()
                .map(|a| {
                    if a.predicate == fact.predicate {
                        faked_fact.clone()
                    } else {
                        a
                    }
                })
                .collect_vec(),
        })
        .collect_vec();
    let faked_history = History::new(faked_iterations, history.leafs);
    let id = InputData::from_history(&faked_history, max_args);
    (faked_history, id, query, rules)
}

/// ## Dataset with simple arithmetic:
/// ```prolog
/// hasAge(alice, 20).
/// isAdult(Alice) :- hasAge(Alice, Age), Age >= 18.
fn simple_arithmetic_dataset<F: PrimeField>() -> (
    History<DATA>,
    InputData<F>,
    Question<String, DATA>,
    Ruleset<String, DATA>,
) {
    let fact = Assertion::from_datalog("hasAge(alice, 20).");
    let rule = ReasonerRule::from_datalog("isAdult(Alice) :- hasAge(Alice, Age), Age >= 18.");
    let rules = Ruleset {
        rules: vec![rule.clone()],
    };

    let mut preliminary_reasoner = Reasoner::default();
    preliminary_reasoner.add_fact(fact);
    preliminary_reasoner.add_rule(rule);
    let query = Question::from_datalog("?- isAdult(alice).");
    let query_res = preliminary_reasoner.query(&query).unwrap();
    let history = query_res.get_history();
    let max_args = get_max_arguments(&history, &rules);
    let id = InputData::from_history(&history, max_args);
    (history, id, query, rules)
}

/// ## Test Dataset with 1 argument in fact and 2 in rules
/// A dataset containing 1 facts and 2 rule:
/// - foo(alice).
/// - buzz(alice, fizz).
///
/// - bar(Fizz) :- foo(Alice), buzz(Alice, Fizz).
///
/// From this dataset can be derived:
/// - bar(fizz).
fn dataset_diff_arguments<F: PrimeField>() -> (
    History<DATA>,
    InputData<F>,
    Question<String, DATA>,
    Ruleset<String, DATA>,
) {
    let facts = <Vec<Assertion<DATA>>>::from_datalog("hasCert(alice).");
    let rules = Ruleset::from_datalog(
        "
        hasCert(Person) :- testCert(Person).\n
        ok(Person) :- hasCert(Person).\n
        ok(Person) :- hasAge(Person, Age), Age < 10.",
    );

    let mut preliminary_reasoner = Reasoner::default();
    facts
        .into_iter()
        .for_each(|f| preliminary_reasoner.add_fact(f));
    rules
        .iter()
        .for_each(|r| preliminary_reasoner.add_rule(r.clone()));

    let query = Question::from_datalog("?- ok(alice).");
    let query_res = preliminary_reasoner.query(&query).unwrap();
    let history = query_res.get_history();
    let max_args = get_max_arguments(&history, &rules);
    let id = InputData::from_history(&history, max_args);
    (history, id, query, rules)
}

/// # Tests
/// ## Test in inital assertion set
/// The query is in the initial assertion set.
#[test]
fn reasoning_in_initial_assertion_set() {
    let (history, input, ptquery, rules) = initial_dataset("?- hasFriend(alice, bob).");
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    assert!(verif);
}

/// ## Test query not entailed
/// A different query is passed to the preliminary reasoner than to the zkp reasoner.
/// Query is not in history.
#[test]
#[should_panic]
fn reasoning_query_not_present() {
    let (history, input, _, rules) = initial_dataset("?- hasFriend(alice, bob).");
    let ptquery = Question::from_datalog("?- fakeQuery(carol, malory).");
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    // Crashes below in test mode because constraints are not satisfied
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    // Crashes below in release mode because verification fails
    assert!(verif);
}

/// ## Test not in initial assertion set
/// A different dataset is passed to zkp than was reasoned about.
/// Query not found in history.
#[test]
#[should_panic]
fn reasoning_not_in_initial_assertion_set() {
    let (_, input, ptquery, rules) = initial_dataset("?- hasWall(alice, wallA).");
    let (history, _, _, _) = initial_dataset_2::<Fr>("?- hasType(apple, fruit).");
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    // Crashes below in test mode because constraints are not satisfied
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    // Crashes below in release mode because verification fails
    assert!(verif);
}

/// ## Test 1 reasoning step
#[test]
fn reasoning_1_reasoning_step() {
    let (history, input, ptquery, rules) = initial_dataset("?- canWrite(bob, wallA).");
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    assert!(verif);
}

/// ## Test missing last history iteration
/// The prereasoner has queried correctly but thrown away the last history
/// iteration. The query is thus not found.
#[test]
#[should_panic]
fn reasoning_missing_last_iter() {
    let (history, input, ptquery, rules) =
        dataset_missing_last_iteration("?- canWrite(bob, wallA).");
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    // Crashes below in test mode because constraints are not satisfied
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    // Crashes below in release mode because verification fails
    assert!(verif);
}

/// ## Test 2 reasoning steps
#[test]
fn reasoning_2_reasoning_steps() {
    let (history, input, ptquery, rules) = two_step_dataset();
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    assert!(verif);
}

/// ## Test Treelike 3 Steps
#[test]
fn reasoning_treelike_3_steps() {
    let (history, input, ptquery, rules) = treelike_dataset_auto(3);
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    assert!(verif);
}

/// ## Test Realistic 2 steps
#[test]
fn reasoning_realistic_2_steps() {
    let (history, input, ptquery, rules) = realistic_dataset("?- canWrite(bob, groupWall).");
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    assert!(verif);
}

/// ## Test invalid 1 step
/// The history is forged and should not be accepted.
#[test]
#[should_panic]
fn reasoning_invalid_1_step() {
    let (history, input, ptquery, rules) = invalid_test_set();
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    assert!(verif);
}

/// # Test simple arithmetic
#[test]
fn arith_simple() {
    let (history, input, ptquery, rules) = simple_arithmetic_dataset();
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    assert!(verif);
}

/// # Test wrong arithmetic
#[test]
#[should_panic]
fn arith_wrong() {
    let (history, input, ptquery, rules) = simple_arithmetic_dataset_wrong();
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    // Crashes below in test mode because constraints are not satisfied
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    // Crashes below in release mode because verification fails
    assert!(verif);
}

/// # Test fact fewer arguments than rules
#[test]
fn fewer_arguments_in_facts() {
    let (history, input, ptquery, rules) = dataset_diff_arguments();
    // Proof setup variables
    let rng = &mut ark_std::rand::rngs::StdRng::seed_from_u64(0u64);
    // Verifier
    let vrf_allocation_data =
        AllocationData::Metadata(IterationMetadata::from_history(&history, &rules));
    let verifier = ReasonerProof {
        input: remove_witness_from_input(&input),
        ptquery: ptquery.clone(),
        rules: rules.clone(),
        allocation_data: vrf_allocation_data,
    };
    // Prover
    let prover = ReasonerProof {
        input,
        ptquery,
        rules,
        allocation_data: AllocationData::History(history),
    };
    let (pk, vk) = Groth16::<Bls12_377>::setup(verifier, rng).unwrap();
    let proof = Groth16::<Bls12_377>::prove(&pk, prover, rng).unwrap();
    // Compare
    let verif = Groth16::<Bls12_377>::verify(&vk, &[], &proof).unwrap();
    assert!(verif);
}
