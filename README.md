# Circuitree
Circuitree is a zero-knowledge proof of knowledge generator designed for rule-based privacy-enhancing applications. The original paper can be found here (open access): https://ieeexplore.ieee.org/document/9718332.

**This code is not yet ready for production environments. Use at your own risk!**

Since the initial article, several improvements have been made to Circuitree:
- Improved performance
- Support for Arkworks, allowing a wide range of proof systems and curves
- Arithmetic arguments and comparisons

## Overview
Circuitree is designed for applications that want to do access control in zero-knowledge. It takes a public Datalog ruleset, secret facts and a target that has to be proven and provides a proof that the rules and facts entail the target.

As an example, we take a privacy-preserving social network with users and walls. `alice` wants to write to `bob`s wall (`bob_wall`). The system has a primitive for this (`canWrite`) and a rule that generates this primitive:
```prolog
canWrite(Alice, BobWall) :- hasFriend(Bob, Alice), hasWall(Bob, BobWall).
```

The prover can then use Circuitree to generate a proof that the secret data `hasFriend(bob, alice)` and `hasWall(bob, bob_wall)` entail `canWrite(alice, bob_wall)`. The verifier can verify this prove without access to the secret data, and can thus prove that Alice is allowed to write to Bob's wall.

A more complex example can be found in circutree/examples/cst directory, where we designed a covid safe ticket in Circuitree.

## Future work
Future work includes:
- Negation: stratified, well-founded semantics or both
- A standard and example for an input gadget that proves authenticity and passes the data on to Circuitree
- Arithmetic operations
